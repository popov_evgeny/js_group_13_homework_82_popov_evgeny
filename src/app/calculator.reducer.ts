import { createReducer, on } from '@ngrx/store';
import { getSymbol } from './calcuator.actions';

const initialState = '0';

export const calculatorReducer = createReducer(
  initialState,
  on(getSymbol, (state, {task}) => {

    if (state === '0'){
      state = '';
    }

    if (task === 'reset') {
      state = '0';
      return state;
    }

    if (task === 'sum'){
      let amount;
      try {
         amount = eval(state);
        if (amount === Infinity) {
          amount = '0'
          return amount;
        }
        return amount;
      } catch (e) {
        amount = '0'
        return amount
      }
    }

    return  state + task;
  }),
)
