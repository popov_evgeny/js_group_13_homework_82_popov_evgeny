import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { getSymbol } from '../calcuator.actions';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.sass']
})
export class CalculatorComponent {
  calcReducer!: Observable<string>;

  constructor(private store: Store<{calcReducer: string}>) {
    this.calcReducer = store.select('calcReducer');
  }

  getSymbol(symbol: string) {
    this.store.dispatch(getSymbol({task: symbol}));
  }
}
