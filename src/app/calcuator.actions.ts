import { createAction, props } from '@ngrx/store';

export const getSymbol = createAction(
  '[Calculator] getSymbol',
  props <{task: string}>()
);

